<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttachFile extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'contact_id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact','contact_id','id');
    }
}
