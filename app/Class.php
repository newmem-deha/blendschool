<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Class extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'grade_id', 'teacher_id'];
}
