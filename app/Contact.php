<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','receiver_id', 'content', 'question_status', 'send_status'];

    public function receiver()
    {
        return $this->belongsTo('App\Receiver','receiver_id','id');
    }

    public function attach_file()
    {
        return $this->hasMany('App\AttachFile','contact_id','id');
    }

    public function question()
    {
        return $this->hasMany('App\Question','contact_id','id');
    }
}
