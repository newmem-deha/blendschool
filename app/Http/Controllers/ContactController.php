<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Receiver;
use App\AttachFile;
use App\Question;

class ContactController extends Controller
{
    public function index()
    {
       return view('layouts.master');
    }

    public function list()
    {
       $receiver = Receiver::all();
       $contacts = Contact::paginate(15);
       return view('list', compact('contacts', 'receiver'));
    }

    public function detail()
    {
       $contacts = Contact::paginate(15);
       return view('detail', compact('contacts'));
    }

    public function create()
    {
       return view('create');
    }

    public function edit(Contact $contact)
    {
       $question = Question::where('contact_id', $contact->id)->get();
       return view('edit', compact('contact', 'question'));
    }
