<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parent extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'dob', 'address', 'phone', 'email', 'student_id'];
}
