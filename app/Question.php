<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['title', 'type', 'content', 'contact_id'];

    public function contact()
    {
        return $this->belongsTo('App\Contact','contact_id','id');
    }
}
