<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiver extends Model
{
    protected $fillable = ['name'];

    public function contact()
    {
        return $this->hasMany('App\Contact','receiver_id','id');
    }
}
