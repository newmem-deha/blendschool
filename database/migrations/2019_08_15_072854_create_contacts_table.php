<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('receiver_id');
            $table->string('title',50);
            $table->string('content',200);
            $table->boolean('question_status')->default(false);
            $table->boolean('send_status')->default(false);
            $table->foreign('receiver_id')
                    ->references('id')
                    ->on('receivers')
                    ->onDelete('cascade');
            $table->softDeletes();
            $table->string('title',50);
            $table->string('content',200);
            $table->boolean('question_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
