<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 39;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('answers')->insert([
                'content' => $faker->text($maxNbChars = 200),
                'question_id' => $faker->numberBetween(1,20),
                'parent_id' => $faker->numberBetween(1,20),
            ]);
        }
        //
    }
}
