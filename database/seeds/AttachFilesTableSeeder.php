<?php

use Illuminate\Database\Seeder;
use App\AttachFile;

class AttachFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = new AttachFile();
        $file->name = "a.txt";
        $file->contact_id = 1;
        $file->save();

        $file = new AttachFile();
        $file->name = "b.txt";
        $file->contact_id = 1;
        $file->save();

        $file = new AttachFile();
        $file->name = "c.txt";
        $file->contact_id = 1;
        $file->save();

        $file = new AttachFile();
        $file->name = "ab.txt";
        $file->contact_id = 2;
        $file->save();

        $file = new AttachFile();
        $file->name = "ac.txt";
        $file->contact_id = 3;
        $file->save();

        $file = new AttachFile();
        $file->name = "abc.txt";
        $file->contact_id = 2;
        $file->save();
        // $faker = Faker\Factory::create();

        // $limit = 20;

        // for ($i = 0; $i < $limit; $i++) {
        //     DB::table('attach_files')->insert([
        //         'link' => $faker->name,
        //         'contact_id' => $faker->numberBetween(1,20),
        //     ]);
        // }
        //
    }
}
