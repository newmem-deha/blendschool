<?php

use Illuminate\Database\Seeder;
use App\Class;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = new Class();
        $class->name = "10A1";
        $class->grade_id = 1;
        $class->teacher_id = 1;
        $class->save();

        $class = new Class();
        $class->name = "11A1";
        $class->grade_id = 2;
        $class->teacher_id = 2;
        $class->save();

        $class = new Class();
        $class->name = "12A1";
        $class->grade_id = 3;
        $class->teacher_id = 3;
        $class->save();

        $class = new Class();
        $class->name = "10A2";
        $class->grade_id = 1;
        $class->teacher_id = 4;
        $class->save();

        $class = new Class();
        $class->name = "11A2";
        $class->grade_id = 2;
        $class->teacher_id = 5;
        $class->save();

        $class = new Class();
        $class->name = "12A2";
        $class->grade_id = 3;
        $class->teacher_id = 6;
        $class->save();
        // $faker = Faker\Factory::create();

        // $limit = 20;

        // for ($i = 0; $i < $limit; $i++) {
        //     DB::table('classes')->insert([
        //         'name' => $faker->title,
        //         'grade_id' => $faker->numberBetween($min = 1, $max = 3),
        //         'teacher_id' => $faker->numberBetween($min = 1, $max = 20),
        //     ]);
        // }
        //
    }
}
