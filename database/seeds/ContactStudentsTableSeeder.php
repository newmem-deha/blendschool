<?php

use Illuminate\Database\Seeder;

class ContactStudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('contact_students')->insert([
                'contact_id' => $faker->numberBetween($min = 1, $max = 20),
                'student_id' => $faker->numberBetween($min = 1, $max = 20),
            ]);
        }
        //
    }
}
