<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $contact = new Contact();
        // $contact->title = "hello";
        // $contact->content = "hello dgd sgs ss svsdv g";
        // $contact->question_status = true;
        // $contact->send_status = false;
        // $contact->receiver_id = 1;
        // $contact->save();

        $faker = Faker\Factory::create();

        $limit = 20;
        $faker = Faker\Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('contacts')->insert([
                'title' => $faker->title,
                'content' => $faker->text,
                'question_status' => rand(0,1),
                'send_status' => rand(0,1),
                'receiver_id'     => $faker->numberBetween(1,5),
                'created_at' => $faker->date($format = 'Y-m-d',$min = '2017-01-22', $max = 'now'),
                'content' => $faker->title,
                'question_status' => 0,
            ]);
        }
    }
}
