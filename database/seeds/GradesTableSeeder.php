<?php

use Illuminate\Database\Seeder;

use App\Grade;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grade = new Grade();
        $grade->name = "Grade 1";
        $grade->save();

        $grade = new Grade();
        $grade->name = "Grade 2";
        $grade->save();

        $grade = new Grade();
        $grade->name = "Grade 3";
        $grade->save();
    }
}
