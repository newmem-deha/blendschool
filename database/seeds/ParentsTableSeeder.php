<?php

use Illuminate\Database\Seeder;

class ParentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('parents')->insert([
                'name' => $faker->name,
                'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'address' => $faker->text($maxNbChars = 100),
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'student_id' => $faker->numberBetween(1,20),
            ]);
        }
        //
    }
}
