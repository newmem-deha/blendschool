<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('questions')->insert([
                'title' => $faker->title,
                'type' => $faker->randomElement($array = array ('yes/no','select','paragraph')),
                'content' => $faker->text($maxNbChars = 200),
                'contact_id' => $faker->numberBetween($min = 1, $max = 20),
            ]);
        }
        //
    }
}
