<?php

use Illuminate\Database\Seeder;
use App\Receiver;

class ReceiverTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $receiver = new Receiver();
        $receiver->name = "All";
        $receiver->save();

        $receiver = new Receiver();
        $receiver->name = "Grade 1";
        $receiver->save();

        $receiver = new Receiver();
        $receiver->name = "Grade 2";
        $receiver->save();

        $receiver = new Receiver();
        $receiver->name = "Grade 3";
        $receiver->save();

        $receiver = new Receiver();
        $receiver->name = "Separation";
        $receiver->save();
    }
}
