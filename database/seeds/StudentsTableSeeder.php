<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('students')->insert([
                'name' => $faker->name,
                'dob' => $faker->date,
                'address' => $faker->text($maxNbChars = 100),
                'phone' => $faker->phoneNumber,
                'class_id' => $faker->numberBetween($min = 1, $max = 20),
            ]);
        }
        //
    }
}
