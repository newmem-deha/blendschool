<?php

use Illuminate\Database\Seeder;
use App\Teacher;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $teacher = new Teacher();
        // $teacher->name = "Nguyen Thi A";
        // $teacher->dob = "1999-05-05";
        // $teacher->address = "Hue";
        // $teacher->phone = "0779900558";
        // $teacher->email = "abc@gmail.com";
        // $teacher->save();

        $teacher = new Teacher();
        $teacher->name = "Nguyen Thi B";
        $teacher->dob = "1998-05-05";
        $teacher->address = "Hue";
        $teacher->phone = "0779900556";
        $teacher->email = "abc1@gmail.com";
        $teacher->save();

        $teacher = new Teacher();
        $teacher->name = "Nguyen Van C";
        $teacher->dob = "1999-05-05";
        $teacher->address = "Hue";
        $teacher->phone = "0779900524";
        $teacher->email = "abc2@gmail.com";
        $teacher->save();

        $teacher = new Teacher();
        $teacher->name = "Nguyen Van D";
        $teacher->dob = "1999-05-05";
        $teacher->address = "Hue";
        $teacher->phone = "0779900524";
        $teacher->email = "abc3@gmail.com";
        $teacher->save();

        $teacher = new Teacher();
        $teacher->name = "Tran Van K";
        $teacher->dob = "1999-05-05";
        $teacher->address = "Hue";
        $teacher->phone = "0779932524";
        $teacher->email = "abc34@gmail.com";
        $teacher->save();

        //
    }
}
