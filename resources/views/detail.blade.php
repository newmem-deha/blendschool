@extends('layouts.master')

@section('content')
<head>
    <style>
            thead tr{
                background-color:#eeeeee;
            }
            #table th {
               padding:16px;
              
           }
           
           #table td {
               padding:16px;
               
           }
       </style>
</head>

<body>
        <div class="container" style="width: 80%; position: relative; left: 105px; top: -610px">
                <div style="margin-top:50px">
                    <div class="btn-group">
                        <button type="button" class="btn btn-light col-md-4">Detail</button>
                        <button type="button" class="btn btn-light col-md-8">Read/Answers</button>
                    </div>
                    <button type="button" style="position: relative; left: 694px; top: 1px;" class="btn btn-primary ">Back</button>
        
                <a href={{route('edit')}} type="button" style="position: relative; left: 702px; top: 1px;" class="btn btn-primary ">Edit</a>
        
                </div>
        
                <div style="margin-top:10px; padding:8px; background-color: darkgray">
                    Setting
                </div>
                <div id="table" style="margin-top:15px">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-2" scope="col">Contact Number</th>
                                <td>First
                                    <a href="" style="margin-left:20%">Delete</a>
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">Date</th>
                                <td>Mark</td>
        
                            </tr>
                            <tr>
                                <th scope="col">Title</th>
                                <td>Jacob</td>
                            </tr>
                            <tr>
                                <th scope="col">Send target</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Status</th>
                                <td>Larry
                                    
                                    <button type="button" style="margin-left:10%" class="btn btn-success">Send Mail</button>
                                    
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">Article</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Attach file</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Register person</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Register date</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Final Update</th>
                                <td>Larry the Bird</td>
                            </tr>
                            <tr>
                                <th scope="col">Last Modified</th>
                                <td>Larry the Bird</td>
                            </tr>
        
                        </thead>
                    </table>
                </div>
            </div>
@endsection