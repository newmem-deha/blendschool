@extends('layouts.master')
@section('content')
<!-- Topbar -->
<div class="container" style="width: 80%; position: relative; left: 105px; top: -610px">  
    <form method="post" enctype="multipart/form-data" action="#">
        <div class="row" style="margin-bottom:40px">
            <div class="col-xl-12">
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label"><b>Send Target: </b></label>&nbsp;
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="target" id="target1" {{$contact->receiver->name==="All" ? "checked":""}} value="option1">
                        <label class="form-check-label" >All</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="target" id="target2" {{in_array($contact->receiver->name,["Grade 1","Grade 2","Grade 3"])? "checked":""}} value="option2">
                        <label class="form-check-label" >Grades</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="target" id="target3" {{$contact->receiver->name==="Separation" ? "checked":""}} value="option3">
                        <label class="form-check-label" >Individual</label>	
                    </div>                                        
                </div>

                <div class="form-group row" id="grade">
                    <label class="col-sm-2 col-form-label"><b>Grades </b></label>&nbsp;
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grade" id="grade10" {{$contact->receiver->name === "Grade 1" ? "checked" : ""}} value="option1">
                        <label class="form-check-label" >Grade 1</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grade" id="grade11" {{$contact->receiver->name === "Grade 2" ? "checked" : ""}} value="option2">
                        <label class="form-check-label" >Grade 2</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="grade" id="grade12" {{$contact->receiver->name === "Grade 3" ? "checked" : ""}} value="option3">
                        <label class="form-check-label" >Grade 3</label>
                    </div>                                        
                </div>
                <div class="form-group row" id="select-student">
                    <label class="col-sm-2 col-form-label"><b>Select Student*</b></label>
                    <div class="col-sm-8">
                        <input required type="button" name="select-student" class="btn btn-primary" value="Select Student">                                    
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label" ><b>Date* </b></label>
                    <div class="col-sm-8">
                        <input required type="date" name="date" class="form-control" value={{$contact->created_at}}>
                    </div>
                </div>                                
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label" ><b>Title* </b></label>
                    <div class="col-sm-8">
                        <input required id="title" type="text" name="title" class="form-control" value={{$contact->title}}>					                    
                    </div>
                </div>
                
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label"><b>Full Text*</b></label>                                        
                    <div class="col-sm-8">
                    <textarea name="fulltext" id="fulltext" cols="30" rows="10" class="form-control" required>{{$contact->content}}</textarea>
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label"><b>Attach File*</b></label>
                    <div class="col-sm-8">
                        <input required type="file" name="attach1" class="form-control" value="Attach File 1">
                        <input required type="file" name="attach2" class="form-control" value="Attach File 2">
                        <input required type="file" name="attach3" class="form-control" value="Attach File 3">
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-sm-2 col-form-label" ><b>Form functions </b></label>&nbsp                                        
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="function" {{$contact->question_status === 0 ? "checked" : ""}} id="notuse" value="option1">
                        <label class="form-check-label" >Not Uses</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="function" {{$contact->question_status === 1 ? "checked" : ""}} id="use" value="option2">
                        <label class="form-check-label" >Uses</label>
                    </div>                                        
                </div> 
                <div class="frmFunction" id="frmFunction">
                    <div class="form-group row frmFunction" >
                        <label class="col-sm-2 col-form-label" ><b>Question* </b></label>
                        <div class="col-sm-8">
                            <input required id="question" type="text" name="question" class="form-control">					                    
                        </div>
                    </div>
                    <div class="form-group row frmFunction" >
                        <label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp                                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input answerType" type="radio" name="answerType" checked id="answerType1" value="option1">
                            <label class="form-check-label" >Yes/No Selection</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input answerType" type="radio" name="answerType" id="answerType2" value="option2">
                            <label class="form-check-label" >Short Question</label>
                        </div> 
                        <div class="form-check form-check-inline ">
                            <input class="form-check-input answerType" type="radio" name="answerType" id="answerType3" value="option1">
                            <label class="form-check-label" >Short Selection</label>
                        </div>
                        <div class="form-check form-check-inline ">
                            <input class="form-check-input answerType4" type="radio" name="answerType" value="option2">
                            <label class="form-check-label" >Multiple Question*</label>
                        </div>                             
                    </div>  
                    <div class="form-group row frmFunction choice">
                        <label class="col-sm-2 col-form-label choice"><b> Choice* </b></label>
                        <div class="col-sm-8 choice" >
                            <input required id="choice1" type="text" name="choice" class="form-control choice" ><br class="choice">
                            <input required id="choice2" type="text" name="choice" class="form-control choice" ><br class="choice">
                            <div class="dynamic_field choice"></div>
                            <button type="button" class="btn btn-warning choice addChoice" id="addChoice">Add a new choice </button> 
                        </div>
                    </div>
                    <div class="form-group row frmFunction" >
                        <label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp                                        
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="required" checked id="required" value="option1">
                            <label class="form-check-label" >Any</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="required" id="required" value="option2">
                            <label class="form-check-label" >Required</label>                           
                        </div>                                        
                    </div>                          
                </div>
                <div id="newQuestion"></div>

                <hr class="frmFunction">    

                <div class="form-group row frmFunction" >
                    <label class="col-sm-2 col-form-label frmFunction" ></label>
                    <div class="col-sm-8 frmFunction">
                          <button type="button" class="btn btn-warning frmFunction" id="addQuestion">Add new question</button>
                    </div>
                </div> 
                <div class="form-group row frmFunction" >
                    <label class="col-sm-2 col-form-label frmFunction" ><b>Answer deadline* </b></label>
                    <div class="col-sm-8 frmFunction">
                        <input required type="date" name="date" class="form-control frmFunction">
                    </div>
                </div>              
                </div>                                                
            </div>
            <input type="submit" name="submit" value="UPDATE" class="btn btn-primary">
            <a href="#" class="btn btn-danger">CANCEL</a> 
        </div>
       
    </form>  
</div>
<!-- End of Topbar -->
@endsection

@section('ajax')
    <script>
        $(document).ready(function(){           
            $("#grade").hide(); 
            $("#select-student").hide();
            $("#target1").click(function(){
                $("#grade").hide();
                $("#select-student").hide(); 
            }); 
            $("#target2").click(function(){
                $("#grade").show();
                $("#select-student").hide(); 
            });     
            $("#target3").click(function(){
                $("#select-student").show(); 
                $("#grade").hide(); 
            }); 
            
            $(".frmFunction").hide(); 
            $("#use").click(function(){
                $(".frmFunction").show(); 
            }); 
            $("#notuse").click(function(){
                $(".frmFunction").hide(); 
            });        
            

            $(".choice").hide();
            $(".answerType").click(function(){
              console.log('an');
              $(".choice").hide();
            });
            
            $(".answerType4").click(function(){
              console.log('hien');
              $(".choice").show();
            });


            // add new input
            var i = 1; 
            $(document).on('click', '.addChoice', function(){            
              i++;     
              html = '<div id="row'+i+'">';
              html += '<input required id="newchoice" type="text" name="choice'+i+'" class="form-control choice-list" >';            
              html += '<button type="button" class="btn btn-danger btn-remove" id="remove'+i+'">X</button></div>';    
              $('.dynamic_field').append(html);                
            });

            $(document).on('click', '.btn-remove', function(){                
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addnewChoice', function(){            
              i++;  
              html =  '<div id="row'+i+'">';
              html += '<input required id="newchoice" type="text" name="choice'+i+'" class="form-control choice-list" >';            
              html += '<button type="button" class="btn btn-danger btn-remove" id="remove'+i+'">X</button></div>';    
              $('.dynamic_field1').append(html);                
            });          

            // add new form function      
            var j = 1;
            $('#addQuestion').click(function(){             
              form = '<div id ="rowQueq'+j+'"><hr>';
              form +='<div class="frmFunction" id="frmFunction'+j+'"><div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Question* </b></label><div class="col-sm-8"><input required id="question'+j+'" type="text" name="question" class="form-control" ></div></div>';            
              form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Type </b></label>&nbsp<div class="form-check form-check-inline"><input class="form-check-input answerType" type="radio" name="answerType'+j+'" checked id="answerType1" value="option1"><label class="form-check-label" >Yes/No Selection</label></div>'; 
              form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" type="radio" name="answerType'+j+'" id="answerType2'+j+'" value="option2"><label class="form-check-label" >Short Question</label></div>';                  
              form +='<div class="form-check form-check-inline"><input class="form-check-input answerType" type="radio" name="answerType'+j+'" id="answerType3'+j+'" value="option1"><label class="form-check-label" >Short Selection</label></div>';
              form +='<div class="form-check form-check-inline"><input class="form-check-input answerType4" type="radio" name="answerType'+j+'" value="option2"><label class="form-check-label" >Multiple Question*</label></div></div>';
              form +='<div class="form-group row frmFunction choice"><label class="col-sm-2 col-form-label choice"><b> Choice* </b></label><div class="col-sm-8" ><input required id="choice" type="text" name="choice" class="form-control choice" ><br><input required id="choice2'+j+'" type="text" name="choice'+j+'" class="form-control choice" ><br><div class="dynamic_field1 choice"></div><button type="button" class="btn btn-warning choice addnewChoice">Add a new choice </button></div></div>';
              form +='<div class="form-group row frmFunction" ><label class="col-sm-2 col-form-label" ><b>Answer Required* </b></label>&nbsp<div class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="required'+j+'" checked value="option1"><label class="form-check-label" >Any</label></div><div class="form-check form-check-inline"><input class="form-check-input" type="radio" name="required'+j+'" value="option2"><label class="form-check-label" >Required</label> </div></div></div>';                           
              form +='<button type="button" class="btn btn-danger frmFunction removeQuestion" ">X</button></div>';                                       
              j++;                        
              $('#newQuestion').append(form);
            });

            $(document).on('click','.removeQuestion', function(){
              $(this).parent('div').remove();
            });
        });
       
    </script>   
@endsection