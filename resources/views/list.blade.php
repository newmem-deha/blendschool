@extends('layouts.master')

@section('content')
<head>
    <style>
            thead tr{
                background-color:#eeeeee;
            }
            #table th {
               padding:16px;
              
           }
           
           #table td {
               padding:16px;
               
           }
       </style>
</head>

<body>
    <div class="container" style="width: 80%; position: relative; left: 105px; top: -610px">
        <div style="margin-top:50px">
            <div class="btn-group">
                @foreach($receiver as $rec)
                    <button type="button" class="btn btn-light col-md-3">{{$rec->name}}</button>
                @endforeach
            </div>
            <button type="button" style="width: 165px; left: 240px; top: -30px;" class="btn btn-primary col-md col-md-offset-7">Create contact</button>

        </div>

        <div style="margin-top:10px" class="btn-group mt-5 pt-5">
            <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
                Transmission status <span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Sent</a></li>
                <li><a href="#">Unsent</a></li>
            </ul>
        </div>

        <table id="table" style="margin-top:30px" class="table table-bordered">
            <thead>
                <tr>
                    <th class="col-md-1" scope="col">Code LL</th>
                    <th class="col-md-1" scope="col">Date</th>
                    <th class="col-md-5" scope="col">Title</th>
                    <th class="col-md-1" scope="col">Target</th>
                    <th class="col-md-1" scope="col">Transmission status</th>
                    <th class="col-md-1" scope="col">Registered person </th>
                    <th class="col-md-2" scope="col">Registered Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                <tr>
                    <td>{{$contact->id}}</th>
                    <td>2019-08-16</td>
                    <td>{{$contact->title}}</td>
                    <td>{{$contact->receiver->name}}</td>
                    <td>{{$contact->send_status === 1 ? 'sent': 'unsent'}}</td>
                    <td>Teacher A</td>
                    <td>{{$contact->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
          
        </table>
        <div>
                <div class="pagination float-right" style="float:right">
                    {{ $contacts->appends(request()->query()) }}
                </div>
        </div>		
    </div>

@endsection