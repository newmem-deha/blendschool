<div class="page-wrapper default-theme sidebar-bg bg1 toggled">
        <nav id="sidebar" class="sidebar-wrapper" style="width: 230px">
            <div class="sidebar-content">
                <!-- sidebar-brand  -->
                <div class="sidebar-item sidebar-brand" style="background-color:#2978BD">

                   <a href="{{route('index')}}"><img style="width:50x; height: 25px; padding-left: 50x;" src="img/blend_w_350.png"></a>
                    <!-- margin: 0 auto -->
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-item sidebar-header d-flex flex-nowrap">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Toho
                            <strong>Smith</strong>
                        </span>
                        <span class="user-role">Manager</span>
                    </div>
                </div>
            
                <div class=" sidebar-item sidebar-menu">
                    <ul>
                        <li class="header-menu">    

                        </li>
                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-bulleted-list-w.png">
                                <span class="menu-text"> Attendance</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Dashboard 1
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Dashboard 2</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Dashboard 3</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-book-stack-w.png">

                                <span class="menu-text">Grade</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Products

                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Orders</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Credit cart</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-school-building-w.png">

                                <span class="menu-text">School</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">General</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Panels</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Tables</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Icons</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Forms</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-assessment-w.png">

                                <span class="menu-text">Learning</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Pie chart</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Line chart</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Bar chart</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Histogram</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-pdf-w2.png">

                                <span class="menu-text">Form</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Google maps</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Open street map</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-kentei-w.png">

                                <span class="menu-text">Certification</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Google maps</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Open street map</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-communication-w.png">

                                <span class="menu-text">Parent contact</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('list') }}">Contact list</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('create') }}">Create new</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="sidebar-dropdown">
                            <a href="{{ route('index') }}#">
                                <img style="width:45px; padding-right:10px" src="img/icon-memo-w.png">

                                <span class="menu-text">Student notes</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="{{ route('index') }}#">Google maps</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('index') }}#">Open street map</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="header-menu">

                        </li>

                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-footer  -->

        </nav>


        <!-- page-content  -->


        <!-- page-content" -->
    </div>
    <!-- page-wrapper -->